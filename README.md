# WaKED-Co_Public

Watch of Knowledge on Emergent Diseases-COVID19


Le projet est né d'une collaboration entre la MAP et le SSA (dont IRBA) pour gagner en efficacité dans la veille scientifique relative au COVID19

Les besoins pour gagner en efficacité sont :

1. Mutualiser la veille,

2. Automatiser la veille autant que possible,

3. Organiser et trier les résultats de la veille pour en faciliter l’utilisation,

4. Enrichir les résultats de la veille en notant la qualité des sources et des documents et en attribuant des labels pour faciliter les recherches individuelles et l’utilisation de la base de veille en datascience,

5. Valoriser la base de veille en l’utilisant pas uniquement comme un outil de veille mais aussi comme une base de données permettant des études sur Covid19 en particulier en utilisant les possibilités offertes par l’intelligence artificielle.


![img](waked.jpg)


# URL de l'application 

https://37f6a31315e44e7c8f1a037a6d558dad.us-central1.gcp.cloud.es.io:9243/

Basic Auth
login : beta_user
mdp : beta_userbeta_user

# Tuto de l'application 

https://drive.google.com/open?id=1IQ9mQQuFhEyazUzBGE0T9rSlG6wDm9M8

# Plus d'information sur la page projet du Hackaton

https://hackcovid19.bemyapp.com/#/projects/5e904e09932a90001bc437c5
import yake
from langdetect import detect
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/keywords', methods=['POST'])
def keywords():
    
  if not request.json or not 'text' in request.json:
    return "Bad query", 400
  
  # Language detection using https://pypi.org/project/langdetect/
  language = detect(request.json['text'])
  
  # max_ngram_size
  if not 'max_ngram_size' in request.json:
    max_ngram_size = 3
  else:
    max_ngram_size = request.json['max_ngram_size']
  
  # deduplication_thresold
  if not 'deduplication_thresold' in request.json:
    deduplication_thresold = 0.85
  else:
    deduplication_thresold = request.json['deduplication_thresold']
    
  # deduplication_algo
  if not 'deduplication_algo' in request.json:
    deduplication_algo = 'seqm'
  else:
    deduplication_algo = request.json['deduplication_algo']
    
  # windowSize
  if not 'windowSize' in request.json:
    windowSize = 4
  else:
    windowSize = request.json['windowSize']
    
  # numOfKeywords
  if not 'numOfKeywords' in request.json:
    numOfKeywords = 5
  else:
    numOfKeywords = request.json['numOfKeywords']
    
  keyword_list = []
  try :
    
    custom_kw_extractor = yake.KeywordExtractor(
      lan=language, 
      n=max_ngram_size, 
      dedupLim=deduplication_thresold,
      dedupFunc=deduplication_algo,
      windowsSize=windowSize,
      top=numOfKeywords,
      features=None)
      
    keywords = custom_kw_extractor.extract_keywords(
      request.json['text'])
    
    for kw in keywords:
      keyword_list.append(kw[0])
      
  except:
      pass
      
  return(jsonify(keyword_list))
    
    
